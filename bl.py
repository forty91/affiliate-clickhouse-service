import os
import uuid

from datetime import datetime

from settings import QUERIES_DIR, TRAFFIC_TABLE_NAME_PREFIX
from utils.clickhouse import ClickhouseClient
from utils.redis import RedisClient, REDIS_DATA_DELIMITER
from utils.template import render_template


class TrafficBridge:
    """
    Response for transferring traffic values from redis to clickhouse.
    """
    def __init__(self):
        self.redis_client = RedisClient()
        self.clickhouse_client = ClickhouseClient()
        self.traffic_entities_count = {}

    def create_table_if_not_exists(self, table_name):
        create_table_request = render_template(
            os.path.join(QUERIES_DIR, 'create_traffic_table.sql'),
            table_name=table_name
        )
        self.clickhouse_client.execute(create_table_request)

    def get_traffic_from_redis(self, key):
        while True:
            value = self.redis_client.lpop(key)
            if value is None:
                break

            if self.traffic_entities_count.get(key, None) is None:
                self.traffic_entities_count[key] = 1
            else:
                self.traffic_entities_count[key] += 1

            promo_id, landing_id, action, user_ip, \
            user_agent, user_token, created_at = value.decode('unicode_escape').split(REDIS_DATA_DELIMITER)

            promo_id = uuid.UUID(promo_id)
            landing_id = uuid.UUID(landing_id) if landing_id else None
            user_token = uuid.UUID(user_token)
            created_at = datetime.fromtimestamp(int(created_at))

            yield promo_id, landing_id, action, user_ip, user_agent, user_token, created_at

    def run(self):
        partners = self.redis_client.keys('*')
        for partner_uuid in partners:
            redis_partner = partner_uuid.decode('unicode_escape')
            table_name = '%s%s' % (TRAFFIC_TABLE_NAME_PREFIX, redis_partner.replace('-', '_'))
            self.create_table_if_not_exists(table_name)

            self.clickhouse_client.execute(
                "INSERT INTO {table_name} VALUES".format(table_name=table_name),
                self.get_traffic_from_redis(redis_partner)
            )
