import argparse
import logging
import time

from bl import TrafficBridge


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Set repeating time in seconds.')
    parser.add_argument('repeat', type=int, help='Repeating in seconds.')
    # parser.add_argument('health-check', type=bool, help='Health check for kubernetes.')
    args = parser.parse_args()

    while True:
        traffic_bridge = TrafficBridge()
        traffic_bridge.run()
        logging.info('inserted: %s' % traffic_bridge.traffic_entities_count)

        time.sleep(args.repeat)
