import redis

from settings import REDIS_HOST, REDIS_PORT, REDIS_DB, REDIS_PASSWORD


REDIS_DATA_DELIMITER = '<=>'


class RedisClient(redis.Redis):
    def __init__(self, host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB, password=REDIS_PASSWORD):
        self.values_count = {}
        super().__init__(host=host, port=port, db=db, password=password)

    def get_values(self, key):
        while True:
            value = self.lpop(key)
            if value is None:
                break

            value = value.decode('unicode_escape').split(REDIS_DATA_DELIMITER)
            value[-1] = int(value[-1])

            if self.values_count.get(key, None) is not None:
                self.values_count[key] += 1
            else:
                self.values_count[key] = 1

            yield value
