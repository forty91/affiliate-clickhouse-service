import os

import clickhouse_driver

from settings import CLICKHOUSE_HOST, CLICKHOUSE_PORT, CLICKHOUSE_USER, CLICKHOUSE_PASSWORD, CLICKHOUSE_DB, QUERIES_DIR
from utils.template import render_template


class ClickhouseClient(clickhouse_driver.Client):
    def __init__(self, host=CLICKHOUSE_HOST, port=CLICKHOUSE_PORT, database=CLICKHOUSE_DB,
                 user=CLICKHOUSE_USER, password=CLICKHOUSE_PASSWORD):
        super().__init__(host=host, port=port, database=database, user=user, password=password)
