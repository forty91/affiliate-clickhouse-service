IMAGE_NAME ?= cyberproject/affiliate-clickhouse-service
IMAGE_TAG ?= develop

.PHONY: build

build:
	docker build -t $(IMAGE_NAME):$(IMAGE_TAG) .
