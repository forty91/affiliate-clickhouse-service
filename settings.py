import logging
import os
from logging import config
from environs import Env
from raven import Client
from raven.handlers.logging import SentryHandler
from raven.transport import RequestsHTTPTransport

env = Env()


REDIS_DB = env.int('REDIS_DB', 1)
REDIS_HOST = env.str('REDIS_HOST', '127.0.0.1')
REDIS_PORT = env.int('REDIS_PORT', 6379)
REDIS_PASSWORD = env.str('REDIS_PASSWORD', 'root')


CLICKHOUSE_HOST = env.str('CLICKHOUSE_HOST', '127.0.0.1')
CLICKHOUSE_PORT = env.str('CLICKHOUSE_PORT', '9000')
CLICKHOUSE_DB = env.str('CLICKHOUSE_DB', 'default')
CLICKHOUSE_USER = env.str('CLICKHOUSE_USER', 'default')
CLICKHOUSE_PASSWORD = env.str('CLICKHOUSE_PASSWORD', '')


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
QUERIES_DIR = os.path.join(BASE_DIR, 'queries')


TRAFFIC_TABLE_NAME_PREFIX = 'partner_traffic_'


SENTRY_DSN = env.str('SENTRY_DSN', None)


from settings_local import *


def log_setup():
    if SENTRY_DSN:
        client = Client(SENTRY_DSN)
        sentry_handler = SentryHandler(client)
        sentry_handler.setLevel(logging.ERROR)

        config.dictConfig({
            'version': 1,
            'disable_existing_loggers': True,

            'formatters': {
                'console': {
                    'format': '[%(asctime)s][%(levelname)s] %(name)s '
                              '%(filename)s:%(funcName)s:%(lineno)d | %(message)s',
                    'datefmt': '%H:%M:%S',
                },
            },

            'handlers': {
                'console': {
                    'level': 'DEBUG',
                    'class': 'logging.StreamHandler',
                    'formatter': 'console'
                },
                'sentry': {
                    'level': 'ERROR',
                    'class': 'raven.handlers.logging.SentryHandler',
                    'dsn': SENTRY_DSN,
                    'transport': RequestsHTTPTransport  # боремся с SNI
                },
            },

            'loggers': {
                '': {
                    'handlers': ['console', 'sentry'],
                    'level': 'DEBUG',
                    'propagate': False,
                },
            }
        })
        logging._handlers['sentry'] = sentry_handler

    else:
        config.dictConfig({
            'version': 1,
            'formatters': {
                'console': {
                    'format': '[%(asctime)s][%(levelname)s] %(name)s '
                              '%(filename)s:%(funcName)s:%(lineno)d | %(message)s',
                    'datefmt': '%H:%M:%S',
                },
            },

            'handlers': {
                'console': {
                    'level': 'DEBUG',
                    'class': 'logging.StreamHandler',
                    'formatter': 'console'
                }
            },

            'loggers': {
                '': {
                    'handlers': ['console'],
                    'level': 'DEBUG',
                    'propagate': False,
                },
            }
        })


log_setup()
