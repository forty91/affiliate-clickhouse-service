FROM cyberproject/python:alpine

ADD . /app/
WORKDIR /app

COPY ["wheels/", "/app/wheels"]

RUN pip install wheels/* && \
    rm -rf wheels

ENTRYPOINT ["python", "server.py"]
CMD [""]
