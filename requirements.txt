#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile --output-file requirements.txt requirements.in
#
clickhouse-driver==0.0.17
environs==4.1.0
jinja2==2.10
markupsafe==1.1.0         # via jinja2
marshmallow==2.18.0       # via environs
python-dotenv==0.10.1     # via environs
pytz==2018.9              # via clickhouse-driver
raven==6.10.0
redis==3.0.1
